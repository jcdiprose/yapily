const md5 = require('md5')

const MARVEL_CHARACTERS_ENDPOINT = process.env.MARVEL_CHARACTERS_ENDPOINT
const MARVEL_PRIV_KEY = process.env.MARVEL_PRIV_KEY
const MARVEL_PUB_KEY = process.env.MARVEL_PUB_KEY

class Marvel {
  constructor() {
    this.characters_endpoint = MARVEL_CHARACTERS_ENDPOINT
    this.private_key = MARVEL_PRIV_KEY
    this.public_key = MARVEL_PUB_KEY
  }

  authenticate = () => {
    const timestamp = Date.now()
    const hash = md5(`${timestamp}${this.private_key}${this.public_key}`)
    return { timestamp, hash }
  }
}

module.exports = Marvel

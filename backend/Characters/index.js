const axios = require('axios')
const md5 = require('md5')

const Marvel = require('../Marvel')

class Characters extends Marvel {
  getPaginated = (req, res) => {
    const { timestamp, hash } = this.authenticate()
    const offset = req.query.offset

    axios
      .get(
        `${this.characters_endpoint}?ts=${timestamp}&apikey=${this.public_key}&hash=${hash}&offset=${offset}&limit=100`
      )
      .then((result) => {
        res.status(200)
        res.send(result.data.data)
      })
      .catch((err) => {
        res.status(err.response.status)
        res.send(err)
      })
  }
}

module.exports = new Characters()

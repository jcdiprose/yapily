import React from 'react'

interface CloseIconProps {
  onClick?: () => void
  className?: string
}

export default (props: CloseIconProps) => {
  return (
    <svg width="1em" height="1em" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path
        d="M2.37708 0.300104L0.255762 2.42142L8.3875 10.5532L-0.000832668 18.9415L2.12047 21.0628L10.5088 12.6745L19.1538 21.3194L21.2751 19.1981L12.6301 10.5532L21.0185 2.16481L18.8972 0.0434867L10.5088 8.43184L2.37708 0.300104Z"
        fill="currentColor"
      />
    </svg>
  )
}

import React from 'react'

interface EditIconProps {
  onClick?: () => void
  className?: string
}

export default (props: EditIconProps) => {
  return (
    <svg width="1em" height="1em" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path
        d="M13.0376 3.53555L1.72389 14.8492L1.47898 17.5433C1.42244 18.1652 1.94347 18.6862 2.56541 18.6297L5.25943 18.3848L16.5731 7.07109L13.0376 3.53555Z"
        fill="currentColor"
      />
      <path
        d="M17.2802 6.36398L19.4016 4.24264C19.7921 3.85212 19.7921 3.21895 19.4016 2.82843L17.2802 0.707109C16.8897 0.316585 16.2566 0.316585 15.866 0.707109L13.7447 2.82845L17.2802 6.36398Z"
        fill="currentColor"
      />
    </svg>
  )
}

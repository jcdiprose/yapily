import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Store } from '../Redux/Store'
import { getAllCharacters } from '../Redux/Characters/Actions'
import loader from '../Assets/marvel-loader.gif'

import './Initialize.scss'

interface InitializeProps {
  children: JSX.Element
}

const Initialize = (props: InitializeProps) => {
  const dispatch = useDispatch()

  const is_loading = useSelector((store: Store) => store.characters.characters_loading)

  useEffect(() => {
    const params = new URLSearchParams(window.location.search)

    const id = params.get('id')
    dispatch(getAllCharacters(id))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (is_loading) {
    return (
      <div className="Marvel-characters__loader">
        <img src={loader} alt="Marvel characters finder is loading" />
        <div className="Marvel-characters__loader-text">LOADING CHARACTERS</div>
      </div>
    )
  }

  return props.children
}

export default Initialize

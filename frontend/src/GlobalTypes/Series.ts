import Generic from './Generic'

interface SingleSeries {
  name: string
  resourceURI: string
}

export default interface Series extends Generic {
  items: SingleSeries[]
}

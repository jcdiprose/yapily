import Generic from './Generic'

export interface Story {
  name: string
  resourceURI: string
  type: 'interiorStory' | 'cover'
}

export default interface Stories extends Generic {
  items: Story[]
}

import Generic from './Generic'

interface Event {
  name: string
  resourceURI: string
}

export default interface Events extends Generic {
  items: Event[]
}

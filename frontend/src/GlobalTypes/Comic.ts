import Generic from './Generic'

interface Comic {
  name: string
  resourceURI: string
}

export default interface Comics {
  items: Comic[]
}

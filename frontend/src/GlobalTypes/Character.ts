import Url from './Url'
import Image from './Image'
import Comics from './Comic'
import Stories from './Stories'
import Events from './Events'
import Series from './Series'

export interface Character {
  id: number
  selected_key: number
  name: string
  description: string
  modified: string
  resourceURI?: string
  urls: Url[]
  thumbnail: Image
  comics: Comics
  stories: Stories
  events: Events
  series: Series
}

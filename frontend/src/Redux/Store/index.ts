import { applyMiddleware, combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import charactersReducer from '../Characters/Reducer'
import { InitialState as CharactersState } from '../Characters/CharactersTypes'
import paginationReducer from '../Pagination/Reducer'
import { InitialState as PaginationState } from '../Pagination/PaginationTypes'

const combined = combineReducers({
  characters: charactersReducer,
  pagination: paginationReducer,
})

export interface Store {
  characters: CharactersState
  pagination: PaginationState
}

/*eslint-disable */
const middleware: any[] = [thunk]
/*eslint-enable */

if (process.env.NODE_ENV === 'development') middleware.push(logger)

export default createStore(combined, composeWithDevTools(applyMiddleware(...middleware)))

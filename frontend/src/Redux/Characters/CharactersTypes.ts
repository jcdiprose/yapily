import { AxiosError } from 'axios'

import {
  CHARACTERS_LOADING,
  CHARACTERS_SUCCESSFUL_FETCH,
  SELECT_CHARACTER,
  FILTER_CHARACTERS,
  SAVE_NEW_NAME,
  CHARACTERS_ERROR_FETCH,
} from './ActionTypes'

import { Character } from '../../GlobalTypes/Character'

export interface InitialState {
  characters_loading: boolean
  all: Character[]
  selected: Character | undefined
  filtered: Character[]
  search_term: string
}

export type CharacterArray = Character[]

interface CharactersLoading {
  type: typeof CHARACTERS_LOADING
  payload: boolean
}

interface CharactersSuccess {
  type: typeof CHARACTERS_SUCCESSFUL_FETCH
  payload: {
    values: CharacterArray[]
    id: number | null
  }
}

interface CharactersFailed {
  type: typeof CHARACTERS_ERROR_FETCH
  payload: AxiosError
}

interface CharactersSelect {
  type: typeof SELECT_CHARACTER
  payload: number
}

interface CharactersFilter {
  type: typeof FILTER_CHARACTERS
  payload: string
}

interface CharactersSaveNewName {
  type: typeof SAVE_NEW_NAME
  payload: {
    name: string
    id: number
  }
}

export type CharactersAction =
  | CharactersLoading
  | CharactersSuccess
  | CharactersSelect
  | CharactersFilter
  | CharactersSaveNewName
  | CharactersFailed

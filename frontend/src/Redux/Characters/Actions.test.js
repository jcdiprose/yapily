import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from './Actions'
import * as types from './ActionTypes'

import DummyData from '../../TestData/dummy'
import DummyDataFlat from '../../TestData/dummy_flat'

import { getCharactersByOffset } from '../../Api'
jest.mock('../../Api')

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Characters actions', () => {
  getCharactersByOffset.mockImplementation((increment) => {
    const inc = increment / 10
    return new Promise((resolve, reject) => resolve(DummyDataFlat.slice(inc, inc + 10)))
  })

  it('fetches all the characters from the Marvel API without a character id', () => {
    const expectedActions = [
      { type: types.CHARACTERS_LOADING },
      { type: types.CHARACTERS_SUCCESSFUL_FETCH, payload: { id: null, values: DummyData } },
    ]
    const store = mockStore({
      characters_loading: true,
      all: [],
      selected: undefined,
      filtered: [],
      search_term: '',
    })

    return store.dispatch(actions.getAllCharacters(null)).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('fetches all the characters from the Marvel API with a set character id', () => {
    const id = 1011334
    const expectedActions = [
      { type: types.CHARACTERS_LOADING },
      { type: types.CHARACTERS_SUCCESSFUL_FETCH, payload: { id, values: DummyData } },
    ]
    const store = mockStore({
      characters_loading: true,
      all: [],
      selected: undefined,
      filtered: [],
      search_term: '',
    })

    return store.dispatch(actions.getAllCharacters(id)).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('sets the selected character', () => {
    const expectedAction = { type: types.SELECT_CHARACTER, payload: 123 }

    expect(actions.setSelectedCharacter(123)).toEqual(expectedAction)
  })

  it('clears the selected character', () => {
    const expectedAction = { type: types.CLEAR_SELECTED_CHARACTER }

    expect(actions.clearSelectedCharacter()).toEqual(expectedAction)
  })
  it('filters the character list', () => {
    const expectedAction = { type: types.FILTER_CHARACTERS, payload: 'iron man' }

    expect(actions.filterCharacters('iron man')).toEqual(expectedAction)
  })
  it('saves a new name for a character', () => {
    const expectedAction = { type: types.SAVE_NEW_NAME, payload: { id: 123, name: 'Jon Calum' } }

    expect(actions.saveNewName(123, 'Jon Calum')).toEqual(expectedAction)
  })
})

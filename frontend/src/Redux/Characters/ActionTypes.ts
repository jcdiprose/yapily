export const CHARACTERS_LOADING = 'characters/loading'
export const CHARACTERS_SUCCESSFUL_FETCH = 'characters/success'
export const CHARACTERS_ERROR_FETCH = 'characters/error'

export const SELECT_CHARACTER = 'characters/select'
export const CLEAR_SELECTED_CHARACTER = 'characters/clear_selected'
export const FILTER_CHARACTERS = 'characters/filter_characters'
export const SAVE_NEW_NAME = 'characters/save_new_name'

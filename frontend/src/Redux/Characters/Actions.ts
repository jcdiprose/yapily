import { Dispatch } from 'redux'
import {
  CHARACTERS_LOADING,
  CHARACTERS_SUCCESSFUL_FETCH,
  CHARACTERS_ERROR_FETCH,
  SELECT_CHARACTER,
  CLEAR_SELECTED_CHARACTER,
  FILTER_CHARACTERS,
  SAVE_NEW_NAME,
} from './ActionTypes'
import { getCharactersByOffset } from '../../Api'
import { AxiosError } from 'axios'

export const getAllCharacters = (id: string | null) => async (dispatch: Dispatch) => {
  dispatch({
    type: CHARACTERS_LOADING,
  })

  const offset = 100
  const total = 1500

  const first_characters = await getCharactersByOffset(0)

  dispatch({
    type: CHARACTERS_SUCCESSFUL_FETCH,
    payload: { values: first_characters, id },
  })

  const rest_of_promises = []

  for (let i = offset; i < total; i += offset) {
    rest_of_promises.push(getCharactersByOffset(i))
  }
  // had to return this for tests..
  return Promise.all(rest_of_promises)
    .then((values: any[]) => {
      dispatch({
        type: CHARACTERS_SUCCESSFUL_FETCH,
        payload: { values, id },
      })
    })
    .catch((err: AxiosError) => {
      dispatch({
        type: CHARACTERS_ERROR_FETCH,
        payload: err,
      })
    })
}

export const setSelectedCharacter = (id: number) => ({
  type: SELECT_CHARACTER,
  payload: id,
})

export const clearSelectedCharacter = () => ({
  type: CLEAR_SELECTED_CHARACTER,
})

export const filterCharacters = (search_term: string) => ({
  type: FILTER_CHARACTERS,
  payload: search_term,
})

export const saveNewName = (id: number, name: string) => ({
  type: SAVE_NEW_NAME,
  payload: { id, name },
})

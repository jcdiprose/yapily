import reducer, { initial_state } from './Reducer'
import * as types from './ActionTypes'

import Dummy from '../../TestData/dummy'
import DummyRenamed from '../../TestData/dummy_renamed'

describe('Characters Reducer', () => {
  it(`Should handle ${types.CHARACTERS_LOADING}`, () => {
    expect(
      reducer(initial_state, {
        type: types.CHARACTERS_LOADING,
      })
    ).toEqual({ ...initial_state, characters_loading: true })
  })

  it(`Should handle ${types.CHARACTERS_SUCCESSFUL_FETCH} without id`, () => {
    expect(
      reducer(initial_state, {
        type: types.CHARACTERS_SUCCESSFUL_FETCH,
        payload: {
          values: Dummy,
          id: null,
        },
      })
    ).toEqual({
      ...initial_state,
      characters_loading: false,
      all: Dummy.flat(),
    })
  })

  it(`Should handle ${types.CHARACTERS_SUCCESSFUL_FETCH} with an id`, () => {
    const character_id = 1010866

    expect(
      reducer(initial_state, {
        type: types.CHARACTERS_SUCCESSFUL_FETCH,
        payload: {
          values: Dummy,
          id: character_id,
        },
      })
    ).toEqual({
      ...initial_state,
      characters_loading: false,
      all: Dummy.flat(),
      selected: Dummy.find((character) => character.id === character_id),
    })
  })

  it(`Should handle ${types.FILTER_CHARACTERS}`, () => {
    const character_name = 'Apocalypse'

    expect(
      reducer(
        { ...initial_state, all: Dummy.flat() },
        {
          type: types.FILTER_CHARACTERS,
          payload: character_name,
        }
      )
    ).toEqual({
      ...initial_state,
      all: Dummy.flat(),
      search_term: character_name,
      filtered: Dummy.flat().filter(
        (character) => character.name.toLocaleLowerCase().indexOf(character_name.toLocaleLowerCase()) !== -1
      ),
    })
  })

  it(`Should handle ${types.SAVE_NEW_NAME} when no search term is present`, () => {
    const id = 1011334
    const name = 'Jon'

    expect(
      reducer(
        { ...initial_state, all: Dummy.flat() },
        {
          type: types.SAVE_NEW_NAME,
          payload: {
            id,
            name,
          },
        }
      )
    ).toEqual({
      ...initial_state,
      all: DummyRenamed,
    })
  })

  it(`Should handle ${types.SAVE_NEW_NAME} when search term is present`, () => {
    const id = 1011334
    const name = 'Jon'
    const search_term = 'Abomination'

    const filtered = DummyRenamed.filter(
      (character) => character.name.toLocaleLowerCase().indexOf(name.toLocaleLowerCase()) !== -1
    )

    expect(
      reducer(
        { ...initial_state, all: Dummy.flat(), filtered, search_term },
        {
          type: types.SAVE_NEW_NAME,
          payload: {
            id,
            name,
          },
        }
      )
    ).toEqual({
      ...initial_state,
      all: DummyRenamed,
      search_term,
      filtered,
    })
  })
})

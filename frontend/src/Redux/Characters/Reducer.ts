import produce, { Draft, enableES5 } from 'immer'
import {
  CHARACTERS_LOADING,
  CHARACTERS_SUCCESSFUL_FETCH,
  SELECT_CHARACTER,
  FILTER_CHARACTERS,
  SAVE_NEW_NAME,
  CHARACTERS_ERROR_FETCH,
} from './ActionTypes'
import { Character } from '../../GlobalTypes/Character'
import { InitialState, CharactersAction } from './CharactersTypes'

enableES5()

export const initial_state: InitialState = {
  characters_loading: true,
  all: [],
  selected: undefined,
  filtered: [],
  search_term: '',
}

export default produce((draft: Draft<InitialState>, action: CharactersAction) => {
  switch (action.type) {
    case CHARACTERS_LOADING:
      draft.characters_loading = true
      break

    case CHARACTERS_SUCCESSFUL_FETCH:
      draft.all.push(
        ...action.payload.values.flat().map((character: Character) => {
          const saved_name = localStorage.getItem(`${character.id}`)

          if (typeof action.payload.id === 'string' && parseInt(action.payload.id, 10) === character.id) {
            draft.selected = { ...character, name: saved_name || character.name }
          }

          if (saved_name) {
            return {
              ...character,
              name: saved_name,
            }
          }
          return character
        })
      )

      draft.characters_loading = false
      break

    case CHARACTERS_ERROR_FETCH:
      console.error(action.payload)
      window.alert(action.payload)
      break

    case SELECT_CHARACTER:
      draft.selected = draft.all.find((character: Character) => character.id === action.payload)
      break

    case FILTER_CHARACTERS:
      draft.search_term = action.payload
      draft.filtered = draft.all.filter(
        (character: Character) => character.name.toLocaleLowerCase().indexOf(action.payload.toLocaleLowerCase()) !== -1
      )
      break

    case SAVE_NEW_NAME:
      if (draft.selected) {
        draft.selected.name = action.payload.name
      }

      draft.all = draft.all.map((character: Character) => {
        if (character.id !== action.payload.id) return character
        return { ...character, name: action.payload.name }
      })

      if (!!draft.search_term) {
        draft.filtered = draft.filtered.map((character: Character) => {
          if (character.id !== action.payload.id) return character
          return { ...character, name: action.payload.name }
        })
      }

      localStorage.setItem(`${action.payload.id}`, action.payload.name)

      break

    default:
      break
  }
}, initial_state)

import { SET_PAGINATION_PAGE } from './ActionTypes'

export const setPaginationPage = (page: number) => ({
  type: SET_PAGINATION_PAGE,
  payload: page,
})

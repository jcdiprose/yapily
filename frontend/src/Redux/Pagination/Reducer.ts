import produce, { Draft, enableES5 } from 'immer'
import { SET_PAGINATION_PAGE } from './ActionTypes'
import { InitialState, PaginationAction } from './PaginationTypes'

enableES5()

const initial_state: InitialState = {
  page: 0,
}

export default produce((draft: Draft<InitialState>, action: PaginationAction) => {
  switch (action.type) {
    case SET_PAGINATION_PAGE:
      draft.page = action.payload
      break

    default:
      break
  }
}, initial_state)

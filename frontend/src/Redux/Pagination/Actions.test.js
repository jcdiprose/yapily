import * as actions from './Actions'
import * as types from './ActionTypes'

describe('Pagination actions', () => {
  it('sets the pagination page', () => {
    const expectedAction = { type: types.SET_PAGINATION_PAGE, payload: 1 }

    expect(actions.setPaginationPage(1)).toEqual(expectedAction)
  })
})

import { SET_PAGINATION_PAGE } from './ActionTypes'

export interface InitialState {
  page: number
}

interface SetPagination {
  type: typeof SET_PAGINATION_PAGE
  payload: number
}

export type PaginationAction = SetPagination

import reducer, { initial_state } from './Reducer'
import * as types from './ActionTypes'

describe('Pagination Reducer', () => {
  it(`Should handle ${types.SET_PAGINATION_PAGE}`, () => {
    expect(
      reducer(initial_state, {
        type: types.SET_PAGINATION_PAGE,
        payload: 1,
      })
    ).toEqual({ ...initial_state, page: 1 })
  })
})

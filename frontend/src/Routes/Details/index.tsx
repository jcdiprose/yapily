import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { Store } from '../../Redux/Store'
import { Story } from '../../GlobalTypes/Stories'
import { clearSelectedCharacter, saveNewName } from '../../Redux/Characters/Actions'
import { Container, Row, Col } from 'react-grid-system'
import Header from '../../Components/Header'
import CloseIcon from '../../Icons/close-icon'
import EditIcon from '../../Icons/edit-icon'
import CorrectIcon from '../../Icons/correct-icon'

import './Details.scss'

const Details = () => {
  const dispatch = useDispatch()
  const history = useHistory()

  const [is_editing_name, setIsEditingName] = useState(false)
  const [new_name, updateNewName] = useState('')
  const character = useSelector((store: Store) => store.characters.selected)

  const goToHome = () => {
    clearSelectedCharacter()
    history.push('/')
  }

  const createImageUrl = () => {
    if (!character) return ''

    const path = character.thumbnail.path
    const ext = character.thumbnail.extension
    const variant = 'standard_fantastic'

    return `${path}/${variant}.${ext}`
  }

  const toggleEditName = () => {
    setIsEditingName((prev: boolean) => !prev)
  }

  const saveEditedName = () => {
    if (new_name.length > 0 && character) {
      dispatch(saveNewName(character.id, new_name))
      toggleEditName()
    }
  }

  if (!character) return null

  const disabled_class = new_name.length === 0 ? 'disabled' : ''

  return (
    <>
      <Header>
        <CloseIcon onClick={goToHome} className="Close-icon" />
      </Header>
      <div className="Details">
        <Container>
          <Row>
            <Col sm={12} lg={8}>
              <h1 className="Details__character-name">
                {is_editing_name ? (
                  <input
                    type="text"
                    className="Details__edit-name"
                    value={new_name}
                    onChange={(e: any) => updateNewName(e.target.value)}
                    autoFocus
                  />
                ) : (
                  <span>{character.name}</span>
                )}
                {is_editing_name ? (
                  <>
                    <CloseIcon onClick={toggleEditName} className="Details__edit-name__icon" />
                    <CorrectIcon onClick={saveEditedName} className={`Details__edit-name__icon ${disabled_class}`} />
                  </>
                ) : (
                  <EditIcon className="Details__edit-name__icon" onClick={toggleEditName} />
                )}
              </h1>
              <img src={createImageUrl()} className="Details__character-image" alt={character.name} />
            </Col>
            <Col sm={12} lg={4}>
              {character.description ? (
                <h2 className="Details__character-title">Description</h2>
              ) : (
                <h2 className="Details__character-title">No Description</h2>
              )}
              {character.description && <p className="Details__character-description">{character.description}</p>}

              {character.stories?.available > 0 ? (
                <h2 className="Details__character-title">Stories</h2>
              ) : (
                <h2 className="Details__character-title">No Stories</h2>
              )}
              <ul className="Details__character-story">
                {character.stories &&
                  character.stories.items.map((story: Story) => {
                    return (
                      <li className="Details__character-story__item" key={story.name}>
                        {story.name}
                      </li>
                    )
                  })}
              </ul>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  )
}

export default Details

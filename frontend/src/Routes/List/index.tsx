import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Store } from '../../Redux/Store'
import Header from '../../Components/Header'
import SearchBar from '../../Components/SearchBar'
import PaginatedList from '../../Components/PaginatedList'
import { Character } from '../../GlobalTypes/Character'
import { setSelectedCharacter } from '../../Redux/Characters/Actions'
import { setPaginationPage } from '../../Redux/Pagination/Actions'

import './List.scss'

const List = () => {
  const dispatch = useDispatch()
  const history = useHistory()

  const [characters_list, setCharactersList] = useState<Character[]>([])
  const characters = useSelector((store: Store) => store.characters.all)
  const filtered_characters = useSelector((store: Store) => store.characters.filtered)
  const should_show_filtered = useSelector((store: Store) => store.characters.search_term)
  const page = useSelector((store: Store) => store.pagination.page)

  useEffect(() => {
    if (should_show_filtered) {
      setCharactersList(filtered_characters)
    } else {
      setCharactersList(characters)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [characters, filtered_characters])

  const showDetails = (id: number) => {
    dispatch(setSelectedCharacter(id))
    history.push(`/details?id=${id}`)
  }

  const toggleSetPaginationPage = (page: number) => {
    dispatch(setPaginationPage(page))
  }

  return (
    <div className="List-page">
      <Header>
        <SearchBar />
      </Header>
      <PaginatedList
        items={characters_list}
        onClick={showDetails}
        itemsPerPage={20}
        setPaginationPage={toggleSetPaginationPage}
        initialPage={page}
      />
    </div>
  )
}

export default List

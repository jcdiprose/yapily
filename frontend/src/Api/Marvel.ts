import { Character } from '../GlobalTypes/Character'

export interface MarvelCharacterResponse {
  count: number
  limit: number
  offset: number
  results: Character[]
  total: number
}

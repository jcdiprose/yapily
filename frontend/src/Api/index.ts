import axios, { AxiosError, AxiosResponse } from 'axios'
import { MarvelCharacterResponse } from './Marvel'

export function getCharactersByOffset(offset: number) {
  return new Promise((resolve, reject) => {
    axios
      .get(`http://localhost:4000/characters?offset=${offset}`)
      .then((result: AxiosResponse<MarvelCharacterResponse>) => {
        resolve(result.data.results)
      })
      .catch((error: AxiosError) => reject(error))
  })
}

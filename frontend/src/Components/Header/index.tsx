import React from 'react'
import './Header.scss'

interface HeaderProps {
  children: JSX.Element
}

const Header = (props: HeaderProps) => {
  return (
    <div className="Header">
      <div className="Header__logo">JON CALUM</div>
      {props.children}
    </div>
  )
}

export default Header

import React from 'react'
import { fireEvent, render, screen, waitForDomChange } from '@testing-library/react'
import PaginatedList from './index'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import { setPaginationPage } from '../../Redux/Pagination/Actions'

import characters from '../../TestData/dummy_flat'

const mockStore = configureStore({})

Element.prototype.scrollTo = () => {}

test("Renders the PaginatedList component with first element '3-D Man'", () => {
  const { getByText } = render(
    <PaginatedList items={characters} onClick={() => {}} itemsPerPage={20} initialPage={0} />
  )

  const firstItem = getByText('3-D Man')
  expect(firstItem).toBeInTheDocument()
})

test("Renders the PaginatedList component with last element 'Ajaxis'", () => {
  const { getByText } = render(
    <PaginatedList items={characters} onClick={() => {}} itemsPerPage={20} initialPage={0} />
  )

  const lastItem = getByText('Ajaxis')
  expect(lastItem).toBeInTheDocument()
})

test("Renders the PaginatedList component without item 21 'Akemi'", () => {
  render(<PaginatedList items={characters} onClick={() => {}} itemsPerPage={20} initialPage={0} />)

  const notAvailableElement = screen.queryByText('Akemi')
  expect(notAvailableElement).not.toBeInTheDocument()
})

test('Should be on the second page of the PaginatedList component', async () => {
  const { getByText } = render(
    <PaginatedList items={characters} onClick={() => {}} itemsPerPage={20} initialPage={1} />
  )

  const firstItem = getByText('Akemi')

  expect(firstItem).toBeInTheDocument()
})

// testing the pagination logic has been ommited because pagination logic is contained within the react-paginate package

import React, { useEffect, useState, createRef } from 'react'
import { Character } from '../../GlobalTypes/Character'
import ReactPaginate from 'react-paginate'

import LArrow from '../../Icons/left-arrow'
import RArrow from '../../Icons/right-arrow'
import SelectArrow from '../../Icons/select-arrow'
import './PaginatedList.scss'

interface PaginatedListProps {
  items: Character[]
  onClick: (index: number) => void
  itemsPerPage: number
  setPaginationPage?: (page: number) => void
  initialPage?: number
}

const PaginatedList = (props: PaginatedListProps) => {
  const ITEMS_PER_PAGE = props.itemsPerPage

  const listPage = createRef<HTMLUListElement>()
  const [page_items, setPageItems] = useState<Character[]>([])

  useEffect(() => {
    if (typeof props.initialPage === 'number') {
      let offset = Math.ceil(props.initialPage * ITEMS_PER_PAGE)
      setPageItems(props.items.slice(offset, offset + ITEMS_PER_PAGE))
    }
  }, [props.items, props.initialPage, ITEMS_PER_PAGE])

  const handlePaginationPageChange = (data: Record<string, number>) => {
    props.setPaginationPage && props.setPaginationPage(data.selected)

    if (listPage.current) {
      listPage.current.scrollTo(0, 0)
    }
  }

  return (
    <div className="Paginated-list">
      <ul className="Paginated-list__page" ref={listPage}>
        {page_items.map((item: Character) => {
          return (
            <li key={item.id} className="Paginated-list__page__item" onClick={() => props.onClick(item.id)}>
              {item.name}
              <SelectArrow />
            </li>
          )
        })}
      </ul>
      <ReactPaginate
        pageCount={Math.ceil(props.items.length / ITEMS_PER_PAGE)}
        pageRangeDisplayed={3}
        marginPagesDisplayed={2}
        onPageChange={handlePaginationPageChange}
        containerClassName={'Paginated-list__pagination'}
        activeClassName={'active'}
        previousLabel={<LArrow />}
        nextLabel={<RArrow />}
        initialPage={props.initialPage}
      />
    </div>
  )
}

export default PaginatedList

import React, { useEffect, useState } from 'react'
import CloseIcon from '../../Icons/close-icon'
import SearchIcon from '../../Icons/search-icon'
import { useDebouncedCallback } from 'use-debounce'
import { useDispatch, useSelector } from 'react-redux'
import { filterCharacters } from '../../Redux/Characters/Actions'
import { setPaginationPage } from '../../Redux/Pagination/Actions'
import { Store } from '../../Redux/Store'

import './SearchBar.scss'

const SearchBar = () => {
  const [is_open, setOpen] = useState(false)
  const [search_value, setSearchValue] = useState('')

  const inputRef = (node: HTMLInputElement) => {
    if (node !== null) {
      if (is_open) {
        node.focus()
      } else {
        node.blur()
      }
    }
  }

  const current_search_term = useSelector((store: Store) => store.characters.search_term)
  const dispatch = useDispatch()

  const debounced = useDebouncedCallback(() => {
    dispatch(setPaginationPage(0))
    dispatch(filterCharacters(search_value))
  }, 500)

  useEffect(() => {
    setSearchValue(current_search_term)
  }, [current_search_term])

  useEffect(() => {
    if (search_value) {
      debounced.callback()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search_value])

  const toggleSearch = () => {
    setOpen((prev) => !prev)
  }

  return (
    <>
      <div className={`SearchBar ${is_open ? 'open' : ''}`}>
        <input
          ref={inputRef}
          className={`SearchBar__input ${is_open ? 'open' : ''}`}
          type="search"
          value={search_value}
          onChange={(e) => setSearchValue(e.target.value)}
          placeholder="Search"
        />
        <div className="SearchBar__icon" onClick={toggleSearch}>
          {is_open ? <CloseIcon /> : <SearchIcon />}
        </div>
      </div>
    </>
  )
}

export default SearchBar

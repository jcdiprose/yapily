import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Store from './Redux/Store'
import InitializeApp from './Utils/Initialize'

import List from './Routes/List'
import Details from './Routes/Details'

import { BASE_PATH, DETAILS_PATH } from './Constants'

import './Styles/App.scss'

const App = () => {
  return (
    <Provider store={Store}>
      <Router>
        <InitializeApp>
          <>
            <Route path={BASE_PATH} exact={true} component={List} />
            <Route path={DETAILS_PATH} exact={true} component={Details} />
          </>
        </InitializeApp>
      </Router>
    </Provider>
  )
}

export default App

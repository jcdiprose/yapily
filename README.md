This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Jon Diprose - Coding Challenge - Marvel API - v1

### Setup

`npm i` in /frontend & /backend

Create .env in /backend with following

```
MARVEL_CHARACTERS_ENDPOINT = "http://gateway.marvel.com/v1/public/characters"
MARVEL_PUB_KEY = "*****************"
MARVEL_PRIV_KEY = "******************************"
```

`npm run start` in backend

`npm run build` in frontend
